/**
 * Where@ App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 05/04/19.
*/

import React, { Component } from 'react';
import { StyleSheet, WebView, View, Platform } from 'react-native';
import SplashScreen from 'react-native-splash-screen'

// main class
export default class Test extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    // component life cycle.
    componentDidMount() {
        // do stuff while splash screen is shown
        // After having done stuff (such as async tasks) hide the splash screen
        setTimeout(() => SplashScreen.hide(), 2000);
    }

    static navigationOptions = {
        //  header: null, // remove header from current screen
        title: 'React Native Testing',
        headerStyle: {
            backgroundColor: '#ffffff',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {

        return (
            // <View style={styles.container} >
            <WebView
                style={styles.WebViewStyle}
                ignoreSslError={true}
                // https://dev.tiffulit.co.il/#/login
                source={{ uri: 'http://dev.tiffulit.co.il/#/login' }}
                //  source={{ uri: 'https://facebook.com' }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                startInLoadingState={true}
                onNavigationStateChange={this.onNavigationStateChange}
            />

            // </View>
        );
    }
}

// styles
const styles = StyleSheet.create({
    WebViewStyle:
    {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        marginTop: (Platform.OS) === 'ios' ? 20 : 0
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    }
});