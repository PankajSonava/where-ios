/**
 * Where@ App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 05/04/19.
*/

import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

// main class
export default class DialogView extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    static navigationOptions = {
        header: null, // remove header from current screen
        title: 'React Native',
        headerStyle: {
            backgroundColor: '#000000',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        return (
            <View style={styles.container} >
                <View style={{ backgroundColor: '#000000' }}>
                    <Text style={[styles.gothamBold, {
                        color: "#D2921C",
                        alignSelf: 'center',
                        padding: 10,
                        fontSize: 18
                    }]}>
                        {'ya estás registrado'.toUpperCase()}
                    </Text>
                </View>
            </View>
        );
    }
}

// styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
    },


// Andorid code for font family starts 
    // gothamBold: {
    //      fontFamily: 'GothamBold'
    // },
    // gothamBook: {
    //     fontFamily: 'GothamBook',
    // }
// Andorid code for font family end

// IOS code for font family starts 
    gothamBold: {
        fontFamily: 'Gotham-Bold'
    },
    gothamBook: {
    fontFamily: 'Gotham-Book',
    }
// IOS code for font family end 

});