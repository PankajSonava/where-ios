/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 02/04/19.
*/

import Login from './Login';
import Maps from './Maps';
import Home from './Home';
import DetailPage from './DetailPage';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ContactList from './ContactList';
import PhoneAuthTest from './PhoneAuthTest';
import DialogView from './DialogView';

// define all the screen with navigations.
const AppNavigator = createStackNavigator({
  HomeScreen: { screen: Home },
  LoginScreen: { screen: Login },
  MapScreen: { screen: Maps },
  DetailPage: { screen: DetailPage },
  ContactList: { screen: ContactList },
  PhoneAuthTest: { screen: PhoneAuthTest },
  DialogView: { screen: DialogView }
}
);
const App = createAppContainer(AppNavigator);
export default App;