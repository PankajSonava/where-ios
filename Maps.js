/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 04/04/19.
*/

import React, { Component } from 'react';
import { StyleSheet, Image, Text, View, TouchableHighlight, TouchableOpacity } from 'react-native';
import MapView from 'react-native-maps';
import firebase from 'react-native-firebase';
import { AsyncStorage } from 'react-native';
import { LoginManager } from "react-native-fbsdk";
import Spinner from 'react-native-loading-spinner-overlay';
import { PermissionsAndroid } from 'react-native';
import Contacts from 'react-native-contacts';
import Pulse from 'react-native-pulse';
import { breakStatement } from '@babel/types';

export default class Maps extends Component {
    // constructor with default states currently we have set static location for testing.
    constructor(props) {
        super(props);
        this.state = {
            initialPosition: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.04,
                longitudeDelta: 0.04,
            },
            isOpen: false,
            latitude: 0.0,
            longitude: 0.0,
            name: '',
            fb_id: '',
            mobile_number: '',
            is_login: '',
            spinner: false,
            markers: [],
            logout_option_visible: false,
            contacts: [],
            friends: [],
            height: 40,
            width: 40,
        }
        // define all funtion and bind.
        this.logOutSession = this.logOutSession.bind(this);
        this.goToLoginScreen = this.goToLoginScreen.bind(this);
        this.getDataFromLocal = this.getDataFromLocal.bind(this);
        this.showLoader = this.showLoader.bind(this);
        this.hideLoader = this.hideLoader.bind(this);
        this.getAllNightBarsFromFirebase = this.getAllNightBarsFromFirebase.bind(this);
        this.markerClick = this.markerClick.bind(this);
        this.isMobileNumberExistFirebase = this.isMobileNumberExistFirebase.bind(this);
        this.option_view = this.option_view.bind(this);
        this.getContactOfPhone = this.getContactOfPhone.bind(this);
        this.update_friend_marker = this.update_friend_marker.bind(this);
        this.getMarkerDifferIndex = this.getMarkerDifferIndex.bind(this);
        this.getDynamicHeightWight = this.getDynamicHeightWight.bind(this);
        this.getDynamicHeightWightNew = this.getDynamicHeightWightNew.bind(this);
    }

// Android code for getting phone contacts starts

// get me contact and check if exist contact in business or not.    
        // getContactOfPhone() {
        //     PermissionsAndroid.request(
        //         PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        //         {
        //             'title': 'Contacts',
        //             'message': 'This app would like to view your contacts.'
        //         }
        //     ).then(() => {
        //         this.showLoader();
        //         Contacts.getAll((err, contactss) => {
        //             if (err === 'denied') {
        //                 // error
        //             } else {
        //                 this.setState({
        //                     spinner: false
        //                 });
        //                 alert("Show Contacts: " + JSON.stringify(contacts));
        //                 // contacts returned in Array

        //                 this.setState({
        //                     contacts: contactss
        //                 })
        //                 this.update_friend_marker();
        //             }
        //         })
        //     })
        // }

    // Android code for getting phone contacts end



 // IOS code for getting phone contacts starts

// get me contact and check if exist contact in business or not.
    getContactOfPhone() {
             this.showLoader();
            Contacts.getAll((err, contactss) => {
                this.setState({
                    spinner: false
                });
                if (err === 'denied') {
                    alert('Contact permission denied, please allow this permission from settings.')
                    this.update_friend_marker();
                    // error
                } else {
                    // contacts returned in Array
                     this.setState({
                         contacts: contactss
                     })
                    this.update_friend_marker();
                }
            })
    }
// IOS code for getting phone contacts end


    markerClick = (index) => {
        // go to detail page.
        this.props.navigation.navigate('DetailPage', {
            details: this.state.markers[index],
            uesrID: this.state.fb_id,
            userName: this.state.name,
            mobileNumber: this.state.mobile_number,
            business_index: index + '',
        });
    }

    static navigationOptions = {
        header: null, // remove header from current screen
        title: 'Bar & Nightclub',
        headerStyle: {
            backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    // save data onto firebase cansol after user login.
    async  getAllNightBarsFromFirebase() {
        await firebase.database().ref('Businesses/').once('value', (data) => {
            const result = JSON.stringify(data);
            //alert(result);
            this.setState({
                markers: JSON.parse(result)
            });
            this.getContactOfPhone();
        });
    }

    goToLoginScreen() {
        LoginManager.logOut();
        this.props.navigation.replace('LoginScreen');
    }

    showLoader() {
        this.setState({
            spinner: true
        });
    }

    hideLoader() {
        firebase.auth().signOut();
        this.setState({
            spinner: false
        });
        this.goToLoginScreen();
    }

    async  isMobileNumberExistFirebase() {
        await firebase.database().ref('Users/' + this.state.fb_id).on('value', (snapshot) => {
            const userObj = snapshot.val();
            let vMobileNumber = '';
            if (typeof userObj.mobile_number != "undefined") {
                vMobileNumber = userObj.mobile_number;
                this.setState({
                    mobile_number: vMobileNumber
                })
                // alert(this.state.mobile_number);
            } else {
                vMobileNumber = '';
            }
            if (vMobileNumber.length == 0) {
                // go to mobile number verification first.
                //alert(this.state.fb_id);
                this.props.navigation.replace('PhoneAuthTest', {
                    uesrID: this.state.fb_id
                });
            }
        });
    }

    // AsyncStorage used for user login status with login data.
    async logOutSession() {
        this.showLoader();
        const user = {
            'fb_id': '',
            'name': '',
            'email': '',
            'is_login': ''
        }
        await AsyncStorage.setItem('user', JSON.stringify(user));
        setTimeout(this.hideLoader, 2000);
    }

    // get the user preferences data from AsyncStorage.
    async  getDataFromLocal() {
        let value = await AsyncStorage.getItem('user');
        let existingUser = JSON.parse(value);
        this.setState({
            name: existingUser.name,
            fb_id: existingUser.fb_id,
            is_login: existingUser.is_login
        });
         //alert(this.state.fb_id);
        if (this.state.fb_id.length > 0) {
            this.isMobileNumberExistFirebase();
        } else {
            alert('N/A');
        }
    }

    // component life cycle. get the user current location.
    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
                var initialRegion = {
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.04,
                    longitudeDelta: 0.04,
                }
                // update state with current region.
                this.setState({
                    initialPosition: initialRegion
                });
            },
            (error) => { alert('current location not found.') },
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }

    // component life cycle.
    componentWillMount() {
        var config = {
            apiKey: "AIzaSyAalyR8kFPic4a-lMudy2nwAweJqp5pRY0",
            authDomain: "whereat-306bc.firebaseapp.com",
            databaseURL: "https://whereat-306bc.firebaseio.com",
            projectId: "whereat-306bc",
            storageBucket: "whereat-306bc.appspot.com",
            messagingSenderId: "862975094795"
        };
        firebase.initializeApp(config);
        this.getAllNightBarsFromFirebase();
        this.getDataFromLocal();
    }

    option_view() {
        if (this.state.logout_option_visible) {
            return (<View>
                <TouchableOpacity style={styles.logout_buttonContainer}>
                    <Text style={styles.welcome}>Welcome to React@</Text>
                    <Text style={styles.welcome}>Hello! {this.state.name}</Text>
                    <Text style={styles.mobile_view}>{this.state.mobile_number}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.logout_buttonContainer} onPress={this.logOutSession}>
                    <Text style={styles.buttonText}>LogOut</Text>
                </TouchableOpacity>
            </View>);
        } else {
            return null;
        }
    }

    async update_friend_marker() {
        var my_friends = [];
       // var vContactStr = JSON.stringify(this.state.contacts);
        var numberArr = [];
        this.state.contacts.forEach(contact => {
            if (contact.phoneNumbers.length > 0) {
               let vNumber=contact.phoneNumbers[0].number;
                numberArr.push(vNumber.replace(/ /g,'').replace(/-/g, '').replace('(','').replace(')',''));
            }
        });

       var vContactStr=numberArr.toString();
        await this.state.markers.map((marker, index) => {
            var business_index = index;
            var customers_temp = [];
            firebase.database().ref('/Businesses/' + business_index + '/customers').once('value')
                .then(snapshot => {

                    snapshot.forEach((item) => {
                        customers_temp.push(item.val());
                    });
                    var result = false;
                    for (var i = 0; i < customers_temp.length; i++) {
                        if (customers_temp[i].mobile_number.length > 0) {
                            var mobile_no = customers_temp[i].mobile_number.replace('+91', '');
                            if (vContactStr.trim().replace(' ', '').includes(mobile_no)) {
                                result = true;
                                break;
                            } else {
                                result = false;
                            }
                        }
                    }
                    my_friends.push({
                        index: business_index,
                        status: result,
                        counts: customers_temp.length
                    });
                    if (business_index + 1 == this.state.markers.length) {
                        this.setState({
                            friends: my_friends
                        })
                        this.render();
                    }
                })
        });
    }

    // event header title  return.
    getMarkerDifferIndex(index) {
        // if (index == 0) {
        return (

            // <View style={{ alignItems: 'center' }}>

            //     <View style={{ alignItems: 'center', height: 80, width: 80, }}>

            //         {/* {this.state.friends.length > 0 && this.state.friends[index].counts > 0 && this.getDynamicHeightWight(index, this.state.friends[index].counts)} */}


            //         <Image
            //             source={require('./src/marker.png')}
            //             style={{ resizeMode: 'contain', height: 50, width: 50 }} />


            //         <Pulse color='#3388EF' style={{ marginBottom: 20 }} 
            //         numPulses={2} diameter={80}></Pulse>


            //     </View>

            //     <View style={styles.container_text_map}>
            //         <Text style={styles.text_list_row}>

            //         </Text>
            //     </View>
            // </View>

            <View style={{ alignItems: 'center' }}>

                <View>

                    {this.state.friends.length > 0 && this.state.friends[index].counts > 0 && this.getDynamicHeightWight(index, this.state.friends[index].counts, this.state.friends[index].status)}

                    {/* {this.state.friends.length > 0 && this.state.friends[index].status ?
                        <Pulse alignSelf='center' color='#3388EF' numPulses={2} diameter={this.state.friends.length > 0 && this.state.friends[index].counts > 0 && this.getDynamicHeightWightNew(index, this.state.friends[index].counts)} speed={60} duration={2000} /> : null} */}
                </View>

                <View style={styles.container_text_map}>
                    <Text style={styles.text_list_row}>

                    </Text>
                </View>
            </View>
        );
        // } else {
        //     return (
        //         <View style={{ alignItems: 'center' }}>

        //             <View style={{ height: 50, width: 50 }}>

        //                 {/* {this.state.friends.length > 0 && this.state.friends[index].counts > 0 && this.getDynamicHeightWight(index, this.state.friends[index].counts)} */}


        //                 <Image
        //                     source={require('./src/marker.png')}
        //                     style={{ resizeMode: 'contain', height: 30, width: 30, }}>
        //                 </Image>

        //                 <Pulse alignSelf='center' color='#3388EF' numPulses={2} diameter={50} speed={60} duration={2000} />}

        //                 {/* {this.state.friends.length > 0 && this.state.friends[index].status ?
        //                     <Pulse alignSelf='center' color='#3388EF' numPulses={2} diameter={this.state.friends.length > 0 && this.state.friends[index].counts > 0 && this.getDynamicHeightWightNew(index, this.state.friends[index].counts)} speed={60} duration={2000} /> : null} */}
        //             </View>

        //             <View style={styles.container_text_map}>
        //                 <Text style={styles.text_list_row}>

        //                 </Text>
        //             </View>
        //         </View>
        //     );
        // }
    }

    getDynamicHeightWightNew(index, count) {
        var height_new = 20;
        if (count >= 0 && count <= 5) {
            height_new = 20 + 10;
            return height_new;

        } else if (count >= 6 && count <= 11) {
            height_new = 20 + 10 * 2;
            return height_new;

        } else if (count >= 12 && count <= 17) {
            height_new = 20 + 10 * 3;
            return height_new;

        } else {
            height_new = 20 + 10 * 4;
            return height_new;
        }
    }
    getDynamicHeightWight(index, count, isStatus) {
        var height_new = 20;
        var width_new = 20;

        height_new = 20 + 20;
        width_new = 20 + 20;
        if (isStatus) {
            return (<Image
                source={require('./src/logo_glow_new.png')}
                style={{ resizeMode: 'contain', height: height_new, width: height_new, }} />
            );
        } else {
            return (<Image
                source={require('./src/marker.png')}
                style={{ resizeMode: 'contain', height: height_new, width: height_new, }} />
            );
        }


        // if (count >= 0 && count <= 5) {
        //     height_new = 20 + 20;
        //     width_new = 20 + 20;
        //     return (<Image
        //         source={require('./src/marker.png')}
        //         style={{ resizeMode: 'contain', height: height_new, width: height_new, }} />
        //     );
        // } else if (count >= 6 && count <= 11) {
        //     // height_new = 20 + 10 * 2;
        //     // width_new = 20 + 10 * 2;
        //     height_new = 20 + 20;
        //     width_new = 20 + 20;
        //     return (<Image
        //         source={require('./src/marker.png')}
        //         style={{ resizeMode: 'contain', height: height_new, width: height_new, }} />
        //     );
        // } else if (count >= 12 && count <= 17) {
        //     // height_new = 20 + 10 * 3;
        //     // width_new = 20 + 10 * 3;
        //     height_new = 20 + 20;
        //     width_new = 20 + 20;
        //     return (<Image
        //         source={require('./src/marker.png')}
        //         style={{ resizeMode: 'contain', height: height_new, width: height_new, }} />
        //     );
        // } else {
        //     // height_new = 20 + 10 * 4;
        //     // width_new = 20 + 10 * 4;
        //     height_new = 20 + 20;
        //     width_new = 20 + 20;
        //     return (<Image
        //         source={require('./src/marker.png')}
        //         style={{ resizeMode: 'contain', height: height_new, width: height_new, }} />
        //     );
        // }
    }

    _gotoCurrentLocation() {
        this.map.animateToRegion({
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            // latitudeDelta: 0.0059397161733585335,
            // longitudeDelta: 0.005845874547958374
            latitudeDelta: 0.04,
            longitudeDelta: 0.04,
        });
    }


    render() {
        // return a parent view
        return (
            <View style={styles.container}>
                <Spinner
                    visible={this.state.spinner}
                    textStyle={styles.spinnerTextStyle}
                />
                {/* map view currently we have set static marker locations */}
                <MapView
                    ref={ref => { this.map = ref; }}
                    style={styles.map}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    zoom={14}
                    region={this.state.initialPosition} >
                    {this.state.markers.map((marker, index) => (
                        <MapView.Marker
                            key={index}
                            coordinate={marker.coordinates != null ? marker.coordinates : { "latitude": 0.0, "longitude": 0.0 }}
                            title={marker.name}
                            description={marker.description}
                            onPress={() => this.markerClick(index)} >
                            <View>


                                {this.getMarkerDifferIndex(index)}

                            </View>

                        </MapView.Marker>

                    ))}

                    {!!this.state.latitude && !!this.state.longitude && <MapView.Marker
                        coordinate={{ "latitude": this.state.latitude, "longitude": this.state.longitude }}
                        title={"Your Location"}
                    />}
                </MapView>

                <View style={styles.container_text}>

                    <TouchableOpacity onPress={() => {
                        var logout_option_visible_new = this.state.logout_option_visible;
                        this.setState({
                            logout_option_visible: !logout_option_visible_new
                        })
                    }} style={styles.logout_buttonContainer_new}>
                        <Image
                            source={require('./src/more.png')}
                            style={{
                                width: 22, height: 22,
                                marginTop: 2, marginBottom: 2, alignSelf: 'flex-end'
                            }}
                        />
                    </TouchableOpacity>
                    {this.option_view()}
                </View>


                <View style={styles.container_plus}>

                    <TouchableOpacity style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 60,
                        height: 60,
                        marginRight: 8,
                        marginBottom: 8,
                    }} onPress={() => {

                        this.map.animateToRegion({
                            latitude: this.state.latitude,
                            longitude: this.state.longitude,
                            // latitudeDelta: 0.0059397161733585335,
                            // longitudeDelta: 0.005845874547958374
                            latitudeDelta: 0.41,
                            longitudeDelta: 0.04,
                        }, 2000);
                    }}>
                        <Image
                            source={require('./src/direction.png')}
                            style={{
                                width: 60, height: 60
                            }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles_new = StyleSheet.create({
    text_list_row: {
        fontSize: 8,
        paddingTop: 8,
        paddingLeft: 4,
        paddingRight: 8,
        paddingBottom: 10,
        backgroundColor: '#FFF',
        color: '#000000',
        fontWeight: 'bold',
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 4,
        borderTopRightRadius: 4,
        borderTopLeftRadius: 2
    },
    photo: {
        position: 'relative',
        height: 40,
        width: 40,
        borderRadius: 40 / 2,
        borderColor: '#ffffff',
        borderWidth: 4
    },
    container_text_map: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: -4
    }
});

// define your styles
const styles = StyleSheet.create({

    bottomView: {
        width: '100%',
        height: 24,
        backgroundColor: '#2980b6',
        //backgroundColor: '#0065A3',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0
    },

    textStyle: {
        color: '#fff',
        fontSize: 10
    },
    text_list_row: {

    },
    photo: {
        position: 'relative',
        height: 80,
        width: 80,
        borderRadius: 80 / 2,
        borderColor: '#ffffff',
        // borderColor: '#2980b6',
        borderWidth: 4
    },
    container_text_map: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: -4
    },
    circle: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        backgroundColor: 'red',
    },
    pinText: {
        color: 'red',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 10,
    },
    container_plus: {
        position: 'absolute',
        right: 0,
        flexDirection: 'column',
        marginLeft: 5,
        marginBottom: 26
    },
    container_text: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        flexDirection: 'column',
        margin: 5
    }, detail_bg: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0
    },
    logout_buttonContainer: {
        backgroundColor: '#2980b6',
        paddingVertical: 2
    },
    logout_buttonContainer_new: {
        backgroundColor: 'transparent',
        paddingVertical: 2
    },

    spinnerTextStyle: {
        color: '#f4511e'
    },

    welcome: {
        color: '#fff',
        fontSize: 14,
        textAlign: 'center',
        margin: 5,
    },
    mobile_view: {
        color: '#fff',
        fontSize: 13,
        textAlign: 'center'
    },

    buttonText: {
        fontSize: 20,
        color: '#ffffff',
        textAlign: 'center',
        fontWeight: '400',
    },
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
});