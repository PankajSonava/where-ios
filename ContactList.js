/**
 * Where@ App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 12/04/19.
*/

// show details of Conatct list
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList } from 'react-native';

export default class ContactList extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    static navigationOptions = {
        header: null, // remove header from current screen
        title: 'Detail Page',
        headerStyle: {
            backgroundColor: '#000000',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        const { navigation } = this.props;
        const contacts = navigation.getParam('contacts', '');
        // alert(JSON.stringify(contacts));
        return (
            <View style={{ backgroundColor: '#000000' }}>
                <View style={{ alignItems: "center" }}>
                    <Text style={[{ fontSize: 18, marginTop: 10, color: '#ffffff', fontWeight: 'bold', textAlign: 'center' }]}>Contact List</Text>
                </View>

                <View style={styles.container} >
                    {/* <View style={{ alignItems: "center" }}>
                        <Text style={[{ fontSize: 22, marginTop: 7, color: '#000000', fontWeight: 'bold', textAlign: 'center' }]}>Contact List</Text>
                    </View> */}
                    <FlatList
                        data={contacts}
                        renderItem={({ item }) =>
                            // <Text style={styles.item}>{item.key}</Text>
                           <TouchableOpacity>
                                <View style={styles.list_row}>
                                    <Image source={{ uri: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png' }} style={styles.photo} />
                                    <View style={styles.container_text}>
                                        <Text style={styles.text_name}>
                                            {item.givenName} {item.familyName}
                                        </Text>
                                        <Text style={styles.text_email}>
                                            {item.phoneNumbers.length==0 ? 'N/A':item.phoneNumbers[0].number}
                                            {/* {item.check_in} */}
                                            {/* {'last check in : 11 Apr 10:30 am'} */}
                                        </Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
     // flex: 1,
        // padding: 12,
        // flexDirection: 'row',
        // alignItems: 'center',
        list_row: {
            flex: 1,
            flexDirection: 'row',
            padding: 10,
            marginLeft: 16,
            marginRight: 16,
            marginTop: 8,
            marginBottom: 8,
            borderRadius: 5,
            backgroundColor: '#FFF',
            elevation: 2,
            alignItems: 'center'
            // flex: 1,
            // padding: 12,
            // flexDirection: 'row',
            // backgroundColor: '#ffffff',
            // alignItems: 'center'
        },
    container: {
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'row',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 80,
        marginTop: 10,
        backgroundColor: '#fff'
    },
    // text: {
    //     marginLeft: 12,
    //     fontSize: 16,
    //     color: '#000000'
    // },

    text_name: {
        fontSize: 15,
        color: '#000000'
    }, 
    text_email: {
        fontSize: 13,
        color: '#333333',
        marginTop: 2
    },

    photo: {
        height: 40,
        width: 40,
        borderRadius: 20,
    }, container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    }
});