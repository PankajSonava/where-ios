/**
 * Where@ App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 09/04/19.
*/

// show details of Bar NightClub
import React, { Component } from 'react';
import { StyleSheet, Text, Alert, View, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import CustomListview from './CustomListview';
import call from 'react-native-phone-call'
import { PermissionsAndroid, StatusBar } from 'react-native';
import Contacts from 'react-native-contacts';
import firebase from 'react-native-firebase';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import { Dialog } from 'react-native-simple-dialogs';

export default class DetailPage extends Component {

    // define all state and function bind the function.
    constructor(props) {
        super(props);
        this.state = {
            time: '',
            enableScrollViewScroll: true,
            contact_no: '',
            customers: [],
            spinner: false,
            events: [],
            dialogVisible: false
        }

        this.checkInAction = this.checkInAction.bind(this);
        this.getCustomerList = this.getCustomerList.bind(this);
        this.getContactOfPhone = this.getContactOfPhone.bind(this);
        this.event_title = this.event_title.bind(this);
    }

    static navigationOptions = {
        header: null, // remove header from current screen
        title: 'Detail Page',
        headerStyle: {
            backgroundColor: '#000000',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    // get user contact listing.
    getContactOfPhone() {
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
                'title': 'Contacts',
                'message': 'This app would like to view your contacts.'
            }
        ).then(() => {
            Contacts.getAll((err, contacts) => {
                if (err === 'denied') {
                    // error
                } else {
                    //   alert("Show Contacts: " + JSON.stringify(contacts));
                    // contacts returned in Array
                    this.props.navigation.navigate('ContactList', {
                        contacts: contacts
                    });
                }
            })
        })
    }

    // asynchronous check in.
    async checkInAction() {
        this.setState({
            spinner: true
        });
        const { navigation } = this.props;
        const details = navigation.getParam('details', '');
        const uesrID = navigation.getParam('uesrID', '');
        const userName = navigation.getParam('userName', '');
        const mobileNumber = navigation.getParam('mobileNumber', '');
        const business_index = navigation.getParam('business_index', '');

        await firebase.database().ref('Businesses').child(business_index).child('customers/' + uesrID).update({
            customer_id: uesrID,
            check_in: this.state.time,
            check_out: '',
            customer_name: userName,
            mobile_number: mobileNumber
        }).then((data) => {
            //success callback
            this.setState({
                spinner: false
            });
            // Works on both iOS and Android
            this.setState({ dialogVisible: true })
            // Alert.alert(
            //     'Where@',
            //     // 'React@',
            //     'Ya estas registrado',
            //     [
            //         {
            //             text: 'OK', onPress: () => {
            //                 this.componentDidMount();
            //                 // go to map after check in.
            //                 // this.props.navigation.navigate('MapScreen');
            //             }
            //         }
            //     ],
            //     { cancelable: false }
            // )
        }).catch((error) => {
            //error callback
            alert('error ', error);
        })
    }

    // get all check in customers.
    async getCustomerList() {
        const { navigation } = this.props;
        const details = navigation.getParam('details', '');
        const uesrID = navigation.getParam('uesrID', '');
        const userName = navigation.getParam('userName', '');
        const business_index = navigation.getParam('business_index', '');

        var customers_temp = [];
        var recentPostsRef = firebase.database().ref('/Businesses/' + business_index + '/customers');
        recentPostsRef.orderByChild('check_in').once('value')
            .then(snapshot => {
                // snapshot.val() is the dictionary with all your keys/values from the '/store' path
                snapshot.forEach((item) => {
                    customers_temp.push(item.val());
                })
                customers_temp.sort(function (a, b) {
                    return a.check_in - b.check_in;
                });
                customers_temp.reverse();
                // alert(customers_temp.length);
                this.setState({
                    customers: customers_temp
                });
            })
    }

    // life cycle method.
    componentDidMount() {
        var that = this;
        //Getting the current date-time with required formate and UTC   
        var date = moment()
            .utcOffset('+05:30')
            .format('DD/MM/YYYY HH:MM:ss');
        //Settign up time to show
        that.setState({ time: date });
        this.getCustomerList();
    }

    // life cycle method.
    componentWillMount() {
        const { navigation } = this.props;
        const details = navigation.getParam('details', '');
        var up_temp = [];
        up_temp.push(details.todays_event);
        details.upcoming_events.forEach(element => {
            up_temp.push(element);
        });
        this.setState({
            events: up_temp
        });
        var config = {
            apiKey: "AIzaSyAalyR8kFPic4a-lMudy2nwAweJqp5pRY0",
            authDomain: "whereat-306bc.firebaseapp.com",
            databaseURL: "https://whereat-306bc.firebaseio.com",
            projectId: "whereat-306bc",
            storageBucket: "whereat-306bc.appspot.com",
            messagingSenderId: "862975094795"
        };
        firebase.initializeApp(config);
    }

    // event header title return.
    event_title(index) {
        if (index == 0) {
            return (<View style={{ alignItems: 'center' }}>
                <Text style={[styles_events.title_header, styles.gothamBold]}>{'ESTA NOCHE'} </Text>
                <View style={styles_events.hairline} />
            </View>);
        } else if (index == 1) {
            return (<View style={{ alignItems: 'center' }}>
                <Text style={[styles_events.title_header, styles.gothamBold]}>{'PRÓXIMOS EVENTOS'} </Text>
                <View style={styles_events.hairline} />
            </View>);
        } else if (index == 1) {
            null;
        }
    }

    onEnableScroll = (value) => {
        if (!value) {
            this.setState({
                enableScrollViewScroll: value,
            });
        } else {
            this.setState({
                enableScrollViewScroll: value,
            });
        }
    };

    okayNew = () => {
        return (
            <View style={styles_events.dot_circle}></View>
        );
    }

    renderSeparator = () => {
            return (
                <View
                    style={{
                        height: 1,
                        width: "90%",
                        // backgroundColor: "#D0D0D0",
                        marginLeft: "10%",
                    }}
                >
                    <Image
                        source={require('./src/lines_down.jpg')}
                        style={{
                            resizeMode: 'contain',
                            width: "90%",
                            marginLeft: "10%",
                        }}
                    />
                </View>
            );
    };


    render() {
        const { navigation } = this.props;
        const details = navigation.getParam('details', '');
        return (
            <View onStartShouldSetResponderCapture={() => {
                this.setState({ enableScrollViewScroll: true });
            }} style={{ backgroundColor: '#000000', marginTop: 20 }} >
                {/* <StatusBar barStyle="light-content" hidden={false} backgroundColor="#000000" translucent={true} /> */}
                <Spinner visible={this.state.spinner} textStyle={styles.spinnerTextStyle} />

                {/* <Dialog
                    visible={this.state.dialogVisible}
                    translucent='#00000000'
                    onTouchOutside={() => this.setState({ dialogVisible: false })} >
                    <View style={{ backgroundColor: '#000000' }}>
                        <Text style={[styles.gothamBold, { color: "#D2921C", alignSelf: 'center', padding: 10, fontSize: 18 }]}>{'ya estás registrado'.toUpperCase()} </Text>
                    </View>
                </Dialog> */}

                <ScrollView scrollEnabled={this.state.enableScrollViewScroll}
                    ref={myScroll => (this._myScroll = myScroll)}>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('MapScreen');
                        }}>
                            <Image
                                source={require('./src/back_arrow.png')}
                                style={{
                                    marginLeft: 10,
                                    width: 24, height: 24,
                                }}
                            />
                        </TouchableOpacity>
                        <Text style={[{ textAlign: 'center', alignSelf: 'center', fontSize: 18, color: '#ffffff' }, styles.gothamBold]}>{details.name.toUpperCase()}</Text>
                        <Image style={{
                            width: 45, height: 45,
                        }}
                        />
                    </View>

                    <View style={{ alignItems: "center" }}>
                        <Image style={styles.banner}
                            source={{ uri: details.image_url }} />
                        {/* <View style={styles.hairline} />
                        <View style={styles.view_container}>
                            <Text style={[{ fontSize: 18, marginTop: 7, color: '#E68F29', fontWeight: 'bold', textAlign: 'center' }, styles.gothamBold]}>Amistades Asistiendo</Text>
                        </View>
                        <View style={styles.hairline} /> */}

                        <Image
                            style={{
                                marginLeft: 10, alignSelf: 'center',
                                marginRight: 10,
                                marginTop: 2, marginBottom: 2, width: '90%', height: 50
                            }}
                            source={require('./src/yyyyy.png')} />

                    </View>


                    {this.state.customers.length > 3 ? <View onStartShouldSetResponderCapture={() => {
                        this.setState({ enableScrollViewScroll: false });
                        if (this._myScroll.contentOffset === 0
                            && this.state.enableScrollViewScroll === false) {
                            this.setState({ enableScrollViewScroll: true });
                        }
                    }} style={styles.container_new_gray} >
                        <View style={styles.container}>

                            <FlatList ItemSeparatorComponent={this.renderSeparator} style={{ marginLeft: 16, marginRight: 20 }} showsHorizontalScrollIndicator={true} maxHight={400} ref={'_mylist'} onTouchStart={() => {
                                this.onEnableScroll(false);
                            }}
                                onMomentumScrollEnd={() => {
                                    this.onEnableScroll(true);
                                }}
                                data={this.state.customers}
                                renderItem={({ item }) =>

                                    <TouchableOpacity>
                                        <View>
                                            <View style={styles.list_row}>
                                                {/* <Image source={require('./src/avtar.png')} style={styles.photo} /> */}
                                                <Image source={{ uri: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png' }} style={styles.photo} />
                                                <View style={styles.container_text}>
                                                    <Text style={[styles.text_list_row, styles.gothamBook]}>
                                                        {item.customer_name}
                                                    </Text>

                                                </View>
                                            </View>
                                            {/* <View style={{
                                                backgroundColor: '#c7c7c7',
                                                height: 1,
                                                width: '82%'
                                            }} /> */}
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                    </View> :
                        <View onStartShouldSetResponderCapture={() => {
                            this.setState({ enableScrollViewScroll: false });
                            if (this._myScroll.contentOffset === 0
                                && this.state.enableScrollViewScroll === false) {
                                this.setState({ enableScrollViewScroll: true });
                            }
                        }} style={styles.container_below_3_items} >
                            <FlatList showsHorizontalScrollIndicator={true} maxHight={400} ref={'_mylist'} onTouchStart={() => {
                                this.onEnableScroll(false);
                            }}
                                onMomentumScrollEnd={() => {
                                    this.onEnableScroll(true);
                                }}
                                data={this.state.customers}
                                renderItem={({ item }) =>

                                    <TouchableOpacity>
                                        <View style={styles.list_row}>
                                            {/* for urer image url in future */}
                                            {/* <Image source={{ uri: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png' }} style={styles.photo} /> */}
                                            {/* <Image source={{ uri: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png' }} style={styles.photo} /> */}
                                            <Image source={require('./src/avtar.png')} style={styles.photo} />

                                            <View style={styles.container_text}>
                                                <Text style={[styles.text_list_row, styles.gothamBook]}>
                                                    {item.customer_name}
                                                </Text>

                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>
                    }

                    <Image
                        style={{
                            marginLeft: 10, alignSelf: 'center',
                            marginRight: 10,
                            marginTop: 2, marginBottom: 2, width: '90%', height: 50
                        }}
                        source={require('./src/eventos.png')} />
                    {/* <View style={{ alignItems: "center" }}><View style={styles.hairline} />
                        <View style={styles.view_container}>
                            <Text style={[{ fontSize: 18, marginTop: 7, color: '#E68F29', fontWeight: 'bold', textAlign: 'center' }]}>EVENTOS</Text>
                        </View>
                        <View style={styles.hairline} /></View> */}


                    {this.state.events.length > 2 ? <View onStartShouldSetResponderCapture={() => {
                        this.setState({ enableScrollViewScroll: false });
                        if (this._myScroll.contentOffset === 0
                            && this.state.enableScrollViewScroll === false) {
                            this.setState({ enableScrollViewScroll: true });
                        }
                    }} style={styles_events.container} >
                        <FlatList style={{ paddingRight: 8 }} showsHorizontalScrollIndicator={true} ref={'_mylist'} onTouchStart={() => {
                            this.onEnableScroll(false);
                        }}
                            onMomentumScrollEnd={() => {
                                this.onEnableScroll(true);
                            }}
                            data={this.state.events}
                            renderItem={({ item, index }) =>
                                <TouchableOpacity>
                                    <View>

                                        {this.event_title(index)}

                                        <View style={styles_events.container_footer}>
                                            <Image source={require('./src/components/images/event_1.png')} style={[styles_events.photo, styles_events.bgContainer]} />
                                            <View style={styles_events.container_text}>

                                                <Text style={[styles_events.description_header, styles.gothamBold]}>{item.title.toUpperCase()}</Text>
                                                <Text style={[styles_events.description, styles.gothamBook]}>On Stage: {item.location_name}</Text>
                                                <Text style={[styles_events.description, styles.gothamBook]}>{item.event_time}</Text>
                                                <Text style={[styles_events.description, styles.gothamBook]}>M: {item.price} H: {item.price}</Text>

                                                <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                                    <Text style={[styles_events.description, styles.gothamBook]}>M: {item.price}</Text>
                                                    <View style={styles_events.dot_circle} />
                                                    <Text style={[styles_events.description, styles.gothamBook]}>H: {item.price}</Text>
                                                </View>

                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            }
                        />
                    </View> : <View onStartShouldSetResponderCapture={() => {
                        this.setState({ enableScrollViewScroll: false });
                        if (this._myScroll.contentOffset === 0
                            && this.state.enableScrollViewScroll === false) {
                            this.setState({ enableScrollViewScroll: true });
                        }
                    }} style={styles_events.container_below_3_items} >

                            <FlatList showsHorizontalScrollIndicator={true} ref={'_mylist'} onTouchStart={() => {
                                this.onEnableScroll(false);
                            }}
                                onMomentumScrollEnd={() => {
                                    this.onEnableScroll(true);
                                }}
                                data={this.state.events}
                                renderItem={({ item, index }) =>
                                    <TouchableOpacity>
                                        <View style={{ alignItems: "center" }}>

                                            {this.event_title(index)}

                                            <View style={styles_events.container_footer}>
                                                <Image source={require('./src/components/images/event_1.png')} style={[styles_events.photo, styles_events.bgContainer]} />
                                                <View style={styles_events.container_text}>
                                                    <Text style={[styles_events.description_header, styles.gothamBold]}>{item.title.toUpperCase()}</Text>
                                                    <Text style={[styles_events.description, styles.gothamBook]}>On Stage: {item.location_name}</Text>
                                                    <Text style={[styles_events.description, styles.gothamBook]}>{item.event_time}</Text>
                                                    <Text style={[styles_events.description, styles.gothamBook]}>M: {item.price} H: {item.price}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                }
                            />
                        </View>}
                    <View style={{
                        backgroundColor: '#FF9F2D',
                        height: 1,
                        marginTop: 2,
                        marginBottom: 2,
                        alignSelf: 'center',
                        width: '90%'
                    }} />
                    <View>
                        <View style={styles.bottom_container} >
                            <TouchableOpacity onPress={this.checkInAction}>
                                <Image style={styles.bgContainerbutton} resizeMode='contain'
                                    source={require('./src/components/images/checkin.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {
                                call({
                                    number: details.contact_no,//'9093900003', // String value with the number to call
                                    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
                                }).catch(console.error)
                            }}>
                                <Image style={styles.bgContainerbutton_right} resizeMode='contain'
                                    source={require('./src/components/images/re.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>

                {this.state.dialogVisible ?
                    <View style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0,
                        justifyContent: 'center', alignSelf: 'center', alignItems: 'stretch'
                    }}>
                        <TouchableOpacity onPress={() => this.setState({ dialogVisible: false })}>
                            <View style={{
                                backgroundColor: '#ffffff', alignItems: 'center',
                                justifyContent: 'center', alignContent: 'center', borderRadius: 15,
                                marginLeft: 20, marginRight: 20, height: 100
                            }}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                }}>
                                    <Image
                                        source={require('./src/right_new.png')}
                                        style={{
                                            width: 80, height: 80,
                                            alignSelf: 'center',
                                            marginLeft: -10
                                        }}
                                    />
                                    <Text style={[styles.gothamBold, {
                                        color: "#D2921C",
                                        alignSelf: 'center',
                                        marginLeft: -30,
                                        fontSize: 16
                                    }]}>
                                        {'ya estás registrado'.toUpperCase()}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View> : null}
            </View>
        );
    }
}

const styles_events = StyleSheet.create({
    dot_circle: {
        width: 4,
        height: 4,
        marginLeft: 8,
        marginRight: 8,
        borderRadius: 4 / 2,
        backgroundColor: '#fff'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        paddingTop: 10,
        paddingBottom: 10,
        height: 340,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#D2921C',
        elevation: 2,
        borderRadius: 20,

    },
    container_below_3_items: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        padding: 10,
        // height: 340,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#D2921C',//D2921C
        elevation: 2,
        borderRadius: 20,
    },
    container_footer: {
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 5,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#D2921C',
    },
    hairline: {
        backgroundColor: '#ffffff',
        height: 1,
        marginTop: 1,
        alignItems: 'center',
        width: 285
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    title_header: {
        fontSize: 16,
        color: '#fff',
        textShadowColor: '#F15F1D',
        textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 2

    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 18,
        justifyContent: 'center',
    },
    description: {
        fontSize: 12,
        color: '#fff',
    },
    description_header: {
        fontSize: 14,
        color: '#fff',
    },
    photo: {
        height: 100,
        width: 100,
    },
    bgContainer: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        overflow: 'hidden'
    }
});

const styles = StyleSheet.create({
    gothamBold: {
        fontFamily: 'Gotham-Bold'
    },
    gothamBook: {
        fontFamily: 'Gotham-Book',
    },

    container_new_gray: {
        flex: 1,
        padding: 3,
        backgroundColor: '#000000',
        borderRadius: 20,
        borderWidth: 2,
        marginLeft: 10,
        marginRight: 10,
        borderColor: '#626262',
    },

    container: {
        flex: 1,
        paddingTop: 10,
        paddingBottom: 5,
        height: 250,
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        borderRadius: 20
    },
    container_below_3_items: {
        flex: 1,
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        backgroundColor: '#fff',
        borderRadius: 20,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
        color: '#ffffff'
    },

    list_row: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 4,
        paddingBottom: 4,
        borderRadius: 5,
        alignItems: 'center'
    }, text_list_row: {
        fontSize: 14,
        color: '#000000'
    }, text_list_row_time: {
        fontSize: 12,
        color: '#5D5D5D'
    },
    photo: {
        height: 50,
        width: 50,
        borderRadius: 25,
    },
    hairline: {
        backgroundColor: '#FF9F2D',
        height: 1,
        marginTop: 10,
        width: '82%'
    },

    banner: {
        height: 170,
        width: '95%',
        borderRadius: 20,
    },
    view_container: {
        width: '90%',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    bottom_container: {
        borderRadius: 15,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        marginTop: 10,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    bgContainerbutton: {
        width: 130,
        height: 40,
        alignItems: 'center',
    },
    bgContainerbutton_right: {
        width: 130,
        height: 40,
        marginLeft: 30,
        alignItems: 'center',
    },

    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 14,
        justifyContent: 'center',
    }, spinnerTextStyle: {
        color: '#f4511e'
    }
});