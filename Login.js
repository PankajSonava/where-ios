/**
 * Where@ App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
 * Created by parkhya on 28/03/19.
*/

import React, { Component } from 'react';
import { TouchableOpacity, Alert, StyleSheet, Image, Text, View } from 'react-native';
import { LoginManager } from "react-native-fbsdk";
import { GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
import { AsyncStorage } from 'react-native';
import InstagramLogin from 'react-native-instagram-login'
import Video from 'react-native-video';
import Spinner from 'react-native-loading-spinner-overlay';

// Login Class
export default class Login extends Component {
  // header section
  static navigationOptions = {
    header: null, // remove header from current screen
    title: 'React Native',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  // constructor with default states.
  constructor(props) {
    super(props);
    this.state = {
      fb_id: '',
      name: '',
      email: '',
      is_login: '',
      spinner: false,
      markers: [],
    }

    // define all funtion and bind.
    this._fbAuthClick = this._fbAuthClick.bind(this);
    this._fbAuthInfoClick = this._fbAuthInfoClick.bind(this);
    this.submitData = this.submitData.bind(this);
    this.saveDataIntoLocal = this.saveDataIntoLocal.bind(this);
    this.goToMapDashboardScreen = this.goToMapDashboardScreen.bind(this);
    this.goToPhoneAuthScreen = this.goToPhoneAuthScreen.bind(this);
    this.instagramInfoDetail = this.instagramInfoDetail.bind(this);
    this.showLoader = this.showLoader.bind(this);
    this.hideLoader = this.hideLoader.bind(this);
  }

  showLoader() {
    this.setState({
      spinner: true
    });
  }

  hideLoader() {
    this.setState({
      spinner: false
    });
  }


  // fb button click action.
  _fbAuthClick() {
    var _this = this;
    LoginManager.logOut();
    LoginManager.setLoginBehavior('web')
    LoginManager.logInWithReadPermissions(["public_profile", "email"]).then(
      function (result) {
        if (result.isCancelled) {
          //alert("Login cancelled")
          console.log("Login cancelled");
        } else {
          //alert("Login success with permissions: " +
          //result.grantedPermissions.toString())
          console.log(
            "Login success with permissions: " +
            result.grantedPermissions.toString()
          );
          //_this.showLoader();
          // call a funtion for getting user info after authentication.
          _this._fbAuthInfoClick();
        }
      },
      function (error) {
        console.log("Login fail with error: " + error);
        alert("Login fail with error: " + error);
      }
    );
  }

  instagramInfoDetail(token) {
    var _this = this;
    fetch('https://api.instagram.com/v1/users/self/?access_token=' + token)
      .then((response) => response.json()).then((responseData) => {
        _this.setState({
          fb_id: responseData.data.id,
          name: responseData.data.username,
          email: '',
          is_login: 'yes'
        });
        this.showLoader();
        this.submitData();
      });
  }

  // funtion for getting user info after authentication.
  _fbAuthInfoClick() {
    //alert("Fb auth info called ")
    var _this = this;
    //Create response callback.
    const _responseInfoCallback = (error, result) => {
      if (error) {
        console.log(error)
        alert('Error fetching data: ' + error.toString());
      } else {
        let email_response = '';
        try {
          email_response = result["email"].toString();
        } catch (e) {
          //alert("error is: " + e);

        }
        _this.setState({
          fb_id: result["id"].toString(),
          name: result["name"].toString(),
          email: email_response,
          is_login: 'yes'
        });
        //alert("UserName is: " + result["name"].toString());
        this.submitData();
      }
    }
    // Create a graph request asking for user information with a callback to handle the response.
    const infoRequest = new GraphRequest(
      '/me?fields=name,email,picture',
      null,
      _responseInfoCallback,
    );
    // Start the graph request.
    new GraphRequestManager().addRequest(infoRequest).start();
  }

  // save data onto firebase cansol after user login.
  async  submitData() {
    
    if (this.email == '') {
      //this.state.email = "demo@gmail.com"
    }
    //alert("email is = "+this.state.email);
    //firebase.database().ref('Users/'+this.state.fb_id).se
    await firebase.database().ref('Users/' + this.state.fb_id).set({
      fb_id: this.state.fb_id,
      name: this.state.name,
      //email: this.state.email,
      mobile_number: '',
      is_login: this.state.is_login
    }).then((data) => {
      //success callback
      //alert("success call");
      this.saveDataIntoLocal();
    }).catch((error) => {
      //error callback
      //alert("error call");
      alert("error", error);
    })
  }

  // AsyncStorage used for user login status with login data.
  async saveDataIntoLocal() {
    //alert("saveData call");
    const user = {
      'fb_id': this.state.fb_id,
      'name': this.state.name,
      'email': this.state.email,
      'is_login': this.state.is_login
    }
    await AsyncStorage.setItem('user', JSON.stringify(user));
    this.hideLoader();
    this.goToPhoneAuthScreen();
  }

  // component life cycle.
  componentWillMount() {
    var config = {
      apiKey: "AIzaSyAalyR8kFPic4a-lMudy2nwAweJqp5pRY0",
      authDomain: "whereat-306bc.firebaseapp.com",
      databaseURL: "https://whereat-306bc.firebaseio.com",
      projectId: "whereat-306bc",
      storageBucket: "whereat-306bc.appspot.com",
      messagingSenderId: "862975094795"
    };
    firebase.initializeApp(config);
  }

  goToMapDashboardScreen() {
    this.props.navigation.replace('MapScreen');
  }

  goToPhoneAuthScreen() {
    // go to mobile number verification first.
    this.props.navigation.replace('PhoneAuthTest', {
      uesrID: this.state.fb_id
    });
  }

  render() {
    // return a parent view
    return (
      <View style={styles.main_container}>
        <Spinner
          visible={this.state.spinner}
          textStyle={styles.spinnerTextStyle}
        />

        {/* video view */}
        <View style={styles.container}>
          {<Video
            source={require('./src/video.mp4')}
            rate={1.0}
            volume={1.0}
            muted={false}
            resizeMode={"cover"}
            repeat
            style={styles.video}
            playInBackground={false}
            playWhenInactive={true}
            onBuffer={this.onBuffer}
            onEnd={this.onEnd}
            onError={this.videoError}
          />}

          <View style={styles.loginContainer}>
            <Image resizeMode="contain" style={styles.logo}
              source={require('./src/components/images/login.png')} />
          </View>

          <View style={styles.imagecontainer}>
            <TouchableOpacity onPress={() => this.refs.instagramLogin.show()}>
              <Image style={styles.bgContainer} resizeMode='contain'
                source={require('./src/components/images/login_insta.png')} />
            </TouchableOpacity>

            <TouchableOpacity onPress={this._fbAuthClick}>
              <Image style={styles.bgContainer} resizeMode='contain'
                source={require('./src/components/images/login_fb.png')} />
            </TouchableOpacity>

          </View>

        </View>

        {/* Instagram app credentials */}
        <InstagramLogin
          ref='instagramLogin'
          clientId='f83abd44623b40c3a7ad95560587ac8f'
          redirectUrl='http://192.168.0.253/mine/instagram/'
          scopes={['basic', 'public_content']}
          onLoginSuccess={(token) => this.instagramInfoDetail(token)}
          onLoginFailure={(data) => console.log(data)}
        />

      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#fff'
  }, bgContainer: {
    marginLeft: 18,
    marginRight: 18,
    marginTop: 18,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  imagecontainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    marginBottom: 50,
  },
  loginContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    marginTop: 180,
  },
  logo: {
    position: 'absolute',
    width: 310,
    height: 100,
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  closeStyle: {
    marginTop: 22,
  },
  btn_container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn_buttonContainer: {
    flex: 1,
  },
  main_container: {
    flex: 1
  },
  container: {
    flexDirection: 'column',
    padding: 20,
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(225,225,225,0.2)',
    marginBottom: 10,
    padding: 10,
    color: '#fff'
  },
  buttonContainer: {
    marginBottom: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: '#ffffff',
    textAlign: 'center',
    fontWeight: '700',
  },
  loginButton: {
    backgroundColor: '#2980b6',
    color: '#fff'
  }, welcome: {
    color: '#000000',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  logout_buttonContainer: {
    backgroundColor: '#2980b6',
    paddingVertical: 15
  }
});