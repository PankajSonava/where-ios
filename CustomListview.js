import React from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';
import CustomRow from './CustomRow';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E68F29',

    },
});


const CustomListview = ({ itemList }) => (
    <View >
        <FlatList keyExtractor={this._keyExtractor}
            data={itemList}
            renderItem={({ item }) => <CustomRow
                title={item.title}
                description={item.description}
                image_url={item.image_url}
            />}
        />

    </View>
);

export default CustomListview;