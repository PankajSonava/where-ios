/**
 * Where@ App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
* Created by parkhya on 05/04/19.
*/

import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { AsyncStorage } from 'react-native';
import SplashScreen from 'react-native-splash-screen'

// main class
export default class Home extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            hasLogin: ''
        }
    }

    static navigationOptions = {
        header: null, // remove header from current screen
        title: 'React Native',
        headerStyle: {
            backgroundColor: '#000000',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    // component life cycle.
    componentDidMount() {
        // do stuff while splash screen is shown
        // After having done stuff (such as async tasks) hide the splash screen
        setTimeout(() => SplashScreen.hide(), 2000);
        this.getDataFromLocal();
    }
    
    // get the user preferences data from AsyncStorage.
    async getDataFromLocal() {
        var is_login = '';
        let value = await AsyncStorage.getItem('user');
        if (value != null) {
            let existingUser = JSON.parse(value);
            is_login = existingUser.is_login;
            if (is_login == 'yes') {
                this.props.navigation.replace('MapScreen');
            } else {
                this.props.navigation.replace('LoginScreen');
            }
        } else {
            this.props.navigation.replace('LoginScreen');
        }
    }

    render() {
        return (
            <View style={styles.container} />
        );
    }
}

// styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
    }
});