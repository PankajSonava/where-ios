/**
 * Created by parkhya on 16/03/19.
*/

import React, { Component } from 'react';
import { View, Button, Text, TextInput, Image, Keyboard, StyleSheet, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';
import Spinner from 'react-native-loading-spinner-overlay';

const successImageUri = 'https://cdn.pixabay.com/photo/2015/06/09/16/12/icon-803718_1280.png';

export default class PhoneAuthTest extends Component {
    // header section
    static navigationOptions = {
        header: null, // remove header from current screen
        title: 'Mobile Verification',
        headerStyle: {
            backgroundColor: '#FFF',
        },
        headerTintColor: '#000000',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    constructor(props) {
        super(props);
        this.unsubscribe = null;
        this.state = {
            user: null,
            message: '',
            codeInput: '',
            phoneNumber: '+91',
            confirmResult: null,
            user_id: '',
            spinner: false
        };
    }

    componentDidMount() {
        this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ user: user.toJSON() });
            } else {
                // User has been signed out, reset the state
                this.setState({
                    user: null,
                    message: '',
                    codeInput: '',
                    phoneNumber: '+91',
                    confirmResult: null,
                });
            }
        });
    }

    componentWillUnmount() {
        if (this.unsubscribe) this.unsubscribe();
    }

    signIn = () => {
        const { phoneNumber } = this.state;
        this.setState({ message: 'Sending code ...' });
        Keyboard.dismiss();
        firebase.auth().signInWithPhoneNumber(phoneNumber)
            .then(confirmResult => this.setState({ confirmResult, spinner: true, message: 'Code has been sent!' }))
            .catch(error => this.setState({ message: `Sign In With Phone Number Error: ${error.message}` }));
    };

    confirmCode = () => {
        const { codeInput, confirmResult } = this.state;

        if (confirmResult && codeInput.length) {
            confirmResult.confirm(codeInput)
                .then((user) => {
                    this.setState({ message: 'Code Confirmed!' });
                })
                .catch(error => this.setState({ message: `Code Confirm Error: ${error.message}` }));
        }
    };

    signOut = (phoneNumber) => {
        // firebase.auth().signOut();
        const { navigation } = this.props;
        const uesrID = navigation.getParam('uesrID', '');
        if (uesrID.length != 0) {
            firebase.database().ref('Users/' + uesrID).update({
                mobile_number: phoneNumber
            }).then(() => {
                this.setState({
                    spinner: false
                })
                //alert('Direct navigate')
                this.props.navigation.replace('MapScreen');
            });
        }
    }

    renderPhoneNumberInput() {
        const { phoneNumber } = this.state;
        return (

            <View style={{ flex: 1, alignItems: 'stretch', backgroundColor: '#000000' }}>

                <View style={{
                    flex: 1,
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center', alignSelf: 'center', alignItems: 'center'
                }}>
                    <Image source={require('./src/locatio_gray_logo.png')} style={{
                        height: 340,
                        width: 340,
                        opacity: 0.9,
                        resizeMode: "contain"

                    }} />
                </View>
                <View style={[{
                    padding: 30, justifyContent: 'center', alignItems: 'stretch'
                }, { flex: 1 }]}>

                    <Text style={[{
                        color: '#ffffff', fontSize: 18,
                        alignSelf: 'center'
                    }, styles.gothamBold]}>{'verificación de telefono'.toUpperCase()}</Text>
                    <TextInput
                        autoFocus
                        style={[{
                            paddingLeft: 30,
                            height: 40, marginTop: 40, fontSize: 16,
                            color: '#ffffff', borderColor: '#D2921C',
                            borderWidth: 1, borderRadius: 10
                        }, styles.gothamBook]}
                        onChangeText={value => this.setState({ phoneNumber: value })}
                        placeholder={'Phone number ... '}
                        keyboardType={'phone-pad'}
                        value={phoneNumber}
                    />
                    <TouchableOpacity style={{
                        alignItems: 'center',
                        backgroundColor: '#D2921C',
                        padding: 10,
                        marginTop: 25,
                        borderRadius: 10
                    }}
                        onPress={this.signIn}>
                        <Text style={[{ color: '#FFF', fontSize: 18 }, styles.gothamBold]}> CONFIRMAR </Text>
                    </TouchableOpacity>

                </View>
            </View>

            // <View style={[{ padding: 25 }, { flex: 1 }]}>

            //     <Text style={[{ marginTop: 20, fontSize: 20, alignSelf: 'center', color: '#000000' }, styles.gothamBold]}>Mobile Verification</Text>

            //     <Image style={[{ marginTop: 20 }, { alignSelf: 'center' }]} resizeMode='contain'
            //         source={require('./src/pin.png')} />

            //     <Text style={[{ marginTop: 20 }, styles.gothamBook]}>Enter phone number:</Text>
            //     <TextInput
            //         autoFocus
            //         style={[{ height: 40, marginTop: 15, marginBottom: 15, borderColor: 'gray', borderWidth: 1 }, styles.gothamBook]}
            //         onChangeText={value => this.setState({ phoneNumber: value })}
            //         placeholder={'Phone number ... '}
            //         keyboardType={'phone-pad'}
            //         value={phoneNumber}
            //     />
            //     <Button style={[styles.gothamBook]} title="Confirm" color="green" onPress={this.signIn} />
            // </View>
        );
    }

    renderMessage() {
        const { message } = this.state;

        if (!message.length) return null;

        return (
            <Text onPress={() => {
                this.setState({
                    message: ''
                })
            }} style={{ padding: 5, backgroundColor: '#000', color: '#fff' }}>{message}</Text>
        );
    }

    renderVerificationCodeInput() {
        const { codeInput } = this.state;

        return (

            <View style={{ flex: 1, alignItems: 'stretch', backgroundColor: '#000000' }}>

                <View style={{
                    flex: 1,
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    justifyContent: 'center', alignSelf: 'center', alignItems: 'center'
                }}>
                    <Image source={require('./src/locatio_gray_logo.png')} style={{
                        height: 340,
                        width: 340,
                        opacity: 0.9,
                        resizeMode: "contain"

                    }} />
                </View>
                <View style={[{
                    padding: 30, justifyContent: 'center', alignItems: 'stretch'
                }, { flex: 1 }]}>

                    <Text style={[{
                        color: '#D2921C', fontSize: 18,
                        alignSelf: 'center'
                    }, styles.gothamBold]}>{'código de verificación'.toUpperCase()}</Text>

                    {/* <TextInput
                        autoFocus
                        style={{ height: 40, marginTop: 15, marginBottom: 15 }}
                        onChangeText={value => this.setState({ codeInput: value })}
                        placeholder={'Code ... '}
                        value={codeInput}
                        keyboardType={'phone-pad'}
                    /> */}

                    <TextInput
                        autoFocus
                        style={[{
                            paddingLeft: 15,
                            height: 40, marginTop: 40, fontSize: 16,
                            color: '#ffffff', borderColor: '#C7C7C7',
                            borderWidth: 1, borderRadius: 10
                        }, styles.gothamBook]}
                       // placeholder={'código... '}
                        // keyboardType={'phone-pad'}
                        //value={'código... '}
                        onChangeText={value => this.setState({ codeInput: value })}
                        placeholder={'código... '}
                        placeholderTextColor = "#ffffff"
                        value={codeInput}
                        keyboardType={'phone-pad'}
                    />
                    <TouchableOpacity style={{
                        alignItems: 'center',
                        backgroundColor: '#878787',
                        padding: 10,
                        marginTop: 25,
                        borderRadius: 10
                    }}
                        onPress={this.confirmCode}>
                        <Text style={[{ color: '#fdfdfd', fontSize: 18 }, styles.gothamBold]}>{'CONFIRMAR código'.toUpperCase()} </Text>
                    </TouchableOpacity>

                </View>
            </View>

            // <View style={{ marginTop: 25, padding: 25 }}>
            //     <Text>Enter verification code below:</Text>
            //     <TextInput
            //         autoFocus
            //         style={{ height: 40, marginTop: 15, marginBottom: 15 }}
            //         onChangeText={value => this.setState({ codeInput: value })}
            //         placeholder={'Code ... '}
            //         value={codeInput}
            //         keyboardType={'phone-pad'}
            //     />
            //     <Button title="Confirm Code" color="#841584" onPress={this.confirmCode} />
            // </View>
        );
    }

    render() {
        const { user, confirmResult } = this.state;
        return (
            <View style={{ flex: 1 }}>

                {!user && !confirmResult && this.renderPhoneNumberInput()}

                {this.renderMessage()}

                {!user && confirmResult && this.renderVerificationCodeInput()}

                {user && (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#000000',
                            flex: 1,
                        }} onPress={this.signOut(user.phoneNumber)}>
                        <Spinner
                            visible={this.state.spinner}
                            textStyle={{ color: '#f4511e' }}
                        />

                    </View>
                )}
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
// Andorid code for font family starts 
    // gothamBold: {
    //      fontFamily: 'GothamBold'
    // },
    // gothamBook: {
    //     fontFamily: 'GothamBook',
    // }
// Andorid code for font family end

// IOS code for font family starts 
    gothamBold: {
        fontFamily: 'Gotham-Bold'
    },
    gothamBook: {
    fontFamily: 'Gotham-Book',
    }
// IOS code for font family end 
})
